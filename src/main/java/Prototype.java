import java.util.ArrayList;
import java.util.List;

public class Prototype {


    public int width(ArrayList<Integer> list) {

        if(list.isEmpty()){
            return 0;
        } else {
            list.remove(0);
            return 1 + width(list);
        }
    }


    public int sum(ArrayList<Integer> numbers)
    {
        if (numbers.isEmpty())
        {
            return 0;
        } else {
            Integer number = numbers.get(0);
            numbers.remove(0);
            return number + sum(numbers);
        }
    }


    public int factorial(int number)
    {
        if (number == 0 || number == 1)
        {
            return 1;
        } else {
            return number * factorial(number - 1);
        }
    }


    public boolean find(ArrayList<Integer> numbers, int number)
    {
        if (numbers.isEmpty()) {
            return false;
        } else {
            if (numbers.get(0) == number){
                return true;
            } else {
                numbers.remove(0);
                return find(numbers,number);
            }
        }
    }


    public int findIndex(ArrayList<Integer> numbers, int number) {

            if (numbers.get(0) == number){
                numbers.remove(0);
                return findIndex(numbers, number);

            } else {
                numbers.remove(0);

                // No lo ha encontrado
                if(numbers.isEmpty()){
                    return -1;
                }

                // Sigue contando porque todavía queda material aquí
                return 1 + findIndex(numbers, number);
            }
    }
}

//ERRORES QUE NOS HAN SURGIDO
//
//El compilador tiene que utilizar la versión 1.8 como mínimo, menor de eso da fallo.