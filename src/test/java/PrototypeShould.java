import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class PrototypeShould {

    @Test
    public void get_the_length() {
       assertThat(new Prototype().width(new ArrayList<Integer>(Arrays.asList(2,5)))).isEqualTo(2);
        assertThat(new Prototype().width(new ArrayList<Integer>(Arrays.asList(4,6,7,9,1)))).isEqualTo(5);
        assertThat(new Prototype().width(new ArrayList<Integer>(Arrays.asList(1)))).isEqualTo(1);
        assertThat(new Prototype().width(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,4,2,3,5,6,43,5)))).isEqualTo(13);
    }

    @Test
    public void sum_total_of_numbers() {
        assertThat(new Prototype().sum(new ArrayList<Integer>(Arrays.asList(2,5)))).isEqualTo(7);
        assertThat(new Prototype().sum(new ArrayList<Integer>(Arrays.asList(2,5,7,8,9,10)))).isEqualTo(41);
    }

    @Test
    public void factorial() {
        assertThat(new Prototype().factorial(5)).isEqualTo(120);
        assertThat(new Prototype().factorial(3)).isEqualTo(6);
        assertThat(new Prototype().factorial(2)).isEqualTo(2);
    }


    @Test
    public void find_a_element_on_list() {
        assertThat(new Prototype().find(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,4,2,3,7,5,6,43,5)), 7)).isTrue();
        assertThat(new Prototype().find(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,4,2,3,7,5,6,43,5)), 0)).isFalse();
    }

    @Test
    public void find_the_position_of_element() {
        assertThat(new Prototype().findIndex(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5)), 234)).isEqualTo(-1);
        assertThat(new Prototype().findIndex(new ArrayList<Integer>(),1)).isEqualTo(-1);
        assertThat(new Prototype().findIndex(new ArrayList<Integer>(Arrays.asList(1,2,3,4,5)), 4)).isEqualTo(3);
    }
}